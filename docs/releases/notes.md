# Releases

IsardVDI development flow is rolling release.

In a rolling release development model, software development is ongoing and new updates are continuously released as soon as they are ready. Instead of waiting for a specific release date to introduce new features and improvements, updates are released as soon as they are developed and tested, allowing users to stay up to date with the latest version of the software at all times.

In a rolling release development model, there are no specific version numbers or major releases like in a traditional software development model. Instead, new updates are identified by their date or build number, and users can upgrade to the latest version at any time.

This approach allows for more frequent updates and faster delivery of new features and bug fixes, but it also requires more testing and quality control to ensure that new updates don't introduce new issues or break existing functionality. It also requires users to be vigilant about keeping their software up to date, as new updates may require additional configuration or adjustments to work properly.

So now you can follow new updates in main branch at [https://gitlab.com/isard/isardvdi/-/commits/main](https://gitlab.com/isard/isardvdi/-/releases)