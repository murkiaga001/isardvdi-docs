# Introducció

Hem organitzat la documentació amb:

- [**Manual d'Usuari**](../user/index.ca/) on trobareu com utilitzar l'IsardVDI per crear escriptoris, plantilles, desplegaments, ...
- [**Manual d'Administrador**](../admin/index.ca) per als rols **gestor** i **administrador** amb la interfície d'administració avançada de gestió per a gestionar-ho tot. Els gestors només podran gestionar amb la seva categoria mentre els administradors ho veuran tot i també elements del sistema com descàrregues o hipervisors.
- [**Manual Instal.lació**](../install/requirements) té detalls tècnics per instal·lar i configurar una plataforma de virtualització IsardVDI completa.
- S'està refent la documentació de l'[**API**](../api/index.ca/), però ara té alguns consells que mostren com fer-ho tot a través de l'API.
- A [**Comunitat**](../community/info.ca/) trobareu enllaços per mantenir el contacte i contribuir al projecte.
- [**Casos d'ús**](../use_cases/index.ca/) és útil per a obtenir idees que pugueu adaptar al vostre cas d'ús.

# IsardVDI

Virtual Desktop Infrastructure (VDI) és una tecnologia que permet als usuaris accedir a escriptoris virtuals que estan allotjats en un servidor remot o infraestructura de núvol. El VDI s'està fent cada vegada més popular en organitzacions, ja que proporciona diversos beneficis sobre els entorns d'escriptori tradicionals, inclosa la millora de la seguretat, la gestió més fàcil i una major flexibilitat per al treball remot.

IsardVDI és una solució VDI de **codi obert** que proporciona una plataforma completa de gestió d'infraestructures d'escriptori virtual. Permet als administradors crear, configurar i gestionar escriptoris virtuals i aplicacions per als usuaris a través de múltiples dispositius, independentment de la seva ubicació. IsardVDI utilitza contenidors Docker per proporcionar una plataforma de virtualització lleugera i escalable, que fa que sigui altament eficient i fàcil de desplegar.

<iframe width="800" height="400" src="https://www.youtube.com/embed/modrGJ_Szyk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Podeu provar el programari a [IsardVDI Gitlab](https://gitlab.com/isard/isardvdi) o obrir-nos un tiquet amb la vostra petició a [Gitlab tiquets d'IsardVDI](https://gitlab.com/isard/isardvdi/-/issues/new).