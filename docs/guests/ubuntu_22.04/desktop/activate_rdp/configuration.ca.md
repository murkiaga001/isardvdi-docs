# Activació de RDP en Ubuntu

## Compartició - Login

A partir de la versió 22 d'Ubuntu ja està integrat el servei de RDP. Per tal d’activar-lo s’ha d’anar a **Configuració - Compartició - Escriptori remot** i en **Autenticació** establir aquest login:

![](../installation/images/sharing-remote_desktop.png)

Ha de coincidir amb les credencials de l'escriptori (podem establir-les editant l'escriptori o a l'hora de crear-lo):

![](images/guest_properties.png)


## Fixar la clau entre reinicis de l’Ubuntu

La clau que posem d’accés a RDP dins l’Ubuntu es regenera en cada reinici si Ubuntu no pot desbloquejar l’“Anell de claus” de l’usuari. Per tal d'arreglar-ho s’ha de reiniciar la clau de l’anell de claus a una clau en blanc.

![](images/contrassenyes_i_claus.png)

Fer clic a **Desbloca** i ens demanarà la clau actual de l’usuari (per defecte per a l’usuari **isard** és **pirineus**).

Llavors amb el botó dret sobre **Entrada** sortirà un desplegable en el qual es fa clic sobre **Canvia la contrassenya** per tal de canviar-la:

![](images/canvia_la_contrassenya.png)

I sense escriure res, s'accepta una nova clau en blanc.

![](images/contrassenya_en_blanc.png)

Això permetrà que Ubuntu en iniciar la sessió de l’usuari pugui desblocar l’anell de claus i, per tant, també tenir accés a la clau actual de RDP, i no es veurà obligat a regenerar-ne una de nova.