# Fedora Workstation 38 install

## Instal·lació del SO

![](installation.images/1.png){width="45%"}
![](installation.images/2.png){width="45%"}

![](installation.images/3.png){width="45%"}
![](installation.images/4.png){width="45%"}


## Isard (rols admin o manager)

**Modificar XML de l'escriptori**

![](../../ubuntu_22.04/desktop/installation/images/modify_xml.png){width="40%"}

```
<driver name="qemu" type="qcow2" cache="unsafe" discard="unmap"/>
```


## Configuració

**Creació de l'usuari 'isard'**

![](installation.images/9.png){width="45%"}
![](installation.images/10.png){width="45%"}

**Inici de sessió - GNOME Xorg**

![](installation.images/xorg_big.png){width="70%"}


### Terminal

**Comandaments bàsic**
```
$ sudo dnf update -y
$ sudo dnf install vim gedit vlc net-tools htop curl wget spice-vdagent qemu-guest-agent
```

**Modificar *fstab***
```
$ sudo vim /etc/fstab
for every "ext4" storage, define "noatime,discard"

UUID=xxxxx-xxxxx-xxxxxx /               ext4    defaults,noatime,discard,errors=remount-ro 0       1
```

**Alliberar espai al sistema**
```
$ cd /
$ sudo fstrim -a -v 
```

**Reduir la memòria swap**
```
$ sudo sysctl vm.swappiness=1
```

**Reduir emmagatzematge de logs**
```
$ sudo vim /etc/systemd/journald.conf
SystemMaxUse=20M
SystemKeepFree=4G
```


### Paràmetres

**Privacitat - Pantalla de bloqueig**

![](installation.images/privacy-lock_screen.png){width="70%"}

**Energia**

![](installation.images/power.png){width="70%"}

**Software - Preferències**

![](installation.images/software-preferences.png){width="70%"}


## Personalització

### Paràmetres

**Fons**

![](installation.images/background.png){width="70%"}

**Usuaris**

![](installation.images/users.png){width="70%"}
