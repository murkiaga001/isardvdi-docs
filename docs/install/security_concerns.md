# IsardVDI Security concerns

By default IsardVDI in *production mode* will open some container ports to the public world as they are required (can be modified in *isardvdi.cfg*)

- **80/TCP**: Redirect HTTP to HTTPS and HTTP CONNECT method to proxy spice clients
- **443/TCP**: Web secure port and HTML5 viewers port
- **9999/TCP**: RDP proxy gateway port to connect RDP clients
- **443/UDP**: Wireguard VPN for client users
- **4443/UDP**: Wireguard VPN for infrastructure hypervisors. Not need to be open in an all-in-one setup.

```
NAME                        SERVICE                     PORTS
isard-api                   isard-api
isard-authentication        isard-authentication
isard-core_worker           isard-core_worker
isard-db                    isard-db
isard-engine                isard-engine
isard-grafana               isard-grafana
isard-grafana-agent         isard-grafana-agent
isard-guac                  isard-guac
isard-hypervisor            isard-hypervisor
isard-loki                  isard-loki
isard-portal                isard-portal                0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp, 0.0.0.0:9999->9999/tcp,
isard-prometheus            isard-prometheus
isard-redis                 isard-redis
isard-scheduler             isard-scheduler
isard-squid                 isard-squid
isard-static                isard-static
isard-stats-cadvisor        isard-stats-cadvisor
isard-stats-go              isard-stats-go
isard-stats-node-exporter   isard-stats-node-exporter
isard-stats-rethinkdb       isard-stats-rethinkdb
isard-storage               isard-storage
isard-vpn                   isard-vpn                   0.0.0.0:443->443/udp, 0.0.0.0:4443->4443/udp,
isard-webapp                isard-webapp
isard-websockify            isard-websockify
```

To apply  a base security and complex infrastructure setups to your installation you have some example scripts for Debian 10 at *sysadm* folder:

- **debian_docker.sh**: This is not a security script, it is only the first thing you should do: install docker & docker-compose. Refer to documentation for other installation sample scripts or better go to the latests documentation for your distro at (https://docs.docker.com/engine/install/)[https://docs.docker.com/engine/install/]
- **debian_firewall.sh**: This is a sample script to install and setup **fail2ban** and manage ports in **firewalld** instead of letting the default docker open the ports.