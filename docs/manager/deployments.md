# Deployments

## Management

Deployments created by users can be managed here. Information about desktops and visibility are shown in the table and in the details desktop parameters and users and groups deployed can be seen.

![](deployments.images/deployment1.png)

## Delete

!!! danger "Action not reversible"

    Currently deleting a deployment and its desktops cannot be undone.
    With the input of [recyclebin Trash](https://gitlab.com/isard/isardvdi/-/merge_requests/1692)
    they can be recovered for a predefined time.

The delete icon at the right of each row allows deleting the deployment, thus the desktops belonging to it.

