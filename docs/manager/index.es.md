# Introducción

## Roles

IsardVDI tiene cuatro tipos diferentes de roles de usuario, cada uno hereda las capacidades de roles más básicos. Esta es la jerarquía desde el usuario básico al administrador:

- **usuario**: Es el usuario básico, generalmente asignado a los estudiantes en las escuelas. Este rol puede crear escritorios a partir de plantillas compartidas con él.
- **avanzado**: Este rol se asigna habitualmente a los profesores de las escuelas. Permite al usuario avanzado crear y compartir plantillas de sus escritorios con otros usuarios. También tiene la capacidad de crear despliegues de escritorio a otros usuarios.
- **gestor**: Tiene las capacidades para acceder a la interfaz de administración web y gestionarlo todo (usuarios, grupos, escritorios, plantillas, medios) dentro de su categoría.
- **administración**: Es capaz de administrar recursos de escritorio, bajadas, hipervisores y todas las capacidades del sistema disponibles.

Aquí hay una vista de mesa más granular de las capacidades permitidas por elementos y acciones:

## Capacidades

| Elemento | Acción | Rol de usuario | rol avanzado | Rol gestor | Rol administrador |
| - | - | - | - | - | - |
| Escritorio | crear        | :material-check: | :material-check: | :material-check: | :material-check: |
| Escritorio | editar          | :material-check: | :material-check: | :material-check: | :material-check: |
| Escritorio | borrar        | :material-check: | :material-check: | :material-check: | :material-check: |
| Escritorio | visor directo | :material-close: | :material-check: | :material-check: | :material-check: |
| | | | | | |
| Plantilla | crear        | :material-close: | :material-check: | :material-check: | :material-check: |
| Plantilla | editar          | :material-close: | :material-check: | :material-check: | :material-check: |
| Plantilla | borrar        | :material-close: | :material-close: | :material-check: | :material-check: |
| | | | | | |
| Despliegue | crear        | :material-close: | :material-check: | :material-check: | :material-check: |
| Despliegue | editar          | :material-close: | :material-check: | :material-check: | :material-check: |
| Despliegue | borrar        | :material-close: | :material-check: | :material-check: | :material-check: |
| Despliegue | videowall     | :material-close: | :material-check: | :material-check: | :material-check: |

## Capacidades en elementos no propios

Los usuarios de *rol avanzado* a *rol administrador* también pueden tener algunas acciones disponibles en los elementos que no son propiedad suya, o para crear elementos a otros usuarios.

Leyenda:

- **despliegue**: El rol puede hacer la acción en los escritorios que pertenecen a sus despliegues de escritorios virtuales a otros usuarios.
- **categoría**: El rol puede hacer la acción solo en los elementos que pertenecen a su categoría.

| Elemento | Acción | Rol de usuario | rol avanzado | Rol gestor | Rol administrador |
| | | | | | |
| Escritorio | editar          | :material-close: | despliegue | categoría | :material-check: |
| Escritorio | borrar        | :material-close: | despliegue | categoría | :material-check: |
| Escritorio | visor directo | :material-close: | despliegue | categoría | :material-check: |
| | | | | | |
| Plantilla | editar          | :material-close: | :material-close: | categoría | :material-check: |
| Plantilla | borrar        | :material-close: | :material-close: | categoría | :material-check: |
| | | | | | |
| Despliegue | editar          | :material-close: | :material-close: | :material-close: | :material-close: |
| Despliegue | borrar        | :material-close: | :material-close: | categoría | :material-check: |
| Despliegue | videowall     | :material-close: | :material-close: | :material-close: | :material-close: |