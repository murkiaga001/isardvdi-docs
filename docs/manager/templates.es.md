# Templates

En esta sección se pueden ver todas las plantillas que se hayan creado de todas las categorías.

![](./domains.images/domains7.png)

Si se pulsa el icono ![[+]](./domains.images/domains5.png), se puede ver más información de la plantilla.

* Hardware: como su nombre indica, es el hardware que se le ha asignado a esta plantilla

* Allows: indica a quién se le ha dado permisos para utilizar la plantilla

* Storage: indica el UUID del disco asociado a la plantilla y su uso

* Media: indica los medios asociados a la plantilla, si hay alguno

![Detalles de la plantilla](./domains.images/domains8.png)

## Enable

Al habilitar una plantilla, se permite a todos los usuarios con los cuales se comparta verla y utilizarla para crear escritorios.

Para habilitar o deshabilitar una plantilla, simplemente se tiene que hacer clic en el check box de la columna "Enabled" de la tabla y confirmar la selección en la notificación emergente

![Checkboxes de las plantillas habilitadas](./domains.images/domains53.png)

Al hacer clic en el botón ![Ocultar desactivados](./domains.images/domains49.png) situado arriba a la derecha, las plantillas que no están habilitadas (visibles para los usuarios con los cuales están compartidas) no se mostrarán en la tabla. Al hacer clic en el botón ![Visualizar Desactivados](./domains.images/domains50.png) se volverán a mostrar.

## Duplicate

Esta opción permite duplicar la plantilla en la base de datos, mientras que se mantiene el disco original.

La duplicación de plantillas se puede realizar varias veces y es útil en algunos casos, como por ejemplo:

- Se quiere compartir la plantilla en otras categorías. Se podría duplicar la plantilla y asignarla a otro usuario en una categoría diferente con permisos de compartición separados para la copia nueva y mantener la versión original

La eliminación de una plantilla duplicada no eliminará la plantilla original.

Al duplicar una plantilla, se puede personalizar la copia nueva:

* Nombre de la nueva plantilla
* Descripción de la nueva plantilla
* Usuario asignado como nuevo propietario de la plantilla
* Habilitar o deshabilitar la visibilidad de la plantilla a los usuarios compartidos
* Seleccionar los usuarios con quienes se quiere compartir la nueva plantilla

![](./domains.images/domains46.png)

## Delete

!!! danger "Acción no reversible"

    Actualmente el borrado de una plantilla y sus escritorios y plantillas derivados no se puede recuperar.
    Con la entrada de la MR de [papelera de reciclaje](https://gitlab.com/isard/isardvdi/-/merge_requests/1692)
    se podrán recuperar durante un tiempo predefinido.

Al eliminar una plantilla, aparecerá un modal que mostrará todos los escritorios creados a partir de ella y cualquier plantilla duplicada. Eliminar la plantilla comportará la eliminación de todos los dominios asociados y sus discos.

![](./domains.images/domains47.png)

!!! Warning Dependencias
    La eliminación de la plantilla comportará la eliminación de todos los dominios asociados y sus discos.

Aun así, si la plantilla a eliminar es una duplicada de otra plantilla, no hay que eliminarlas todas. Solo se tendrían que eliminar si también se elimina la plantilla de origen.

![](./domains.images/domains48.png)

## Plantilla como Servidor

Una plantilla que se marque como *servidor* hará que todos los escritorios que se creen sean también servidores. Esto quiere decir que el sistema iniciará siempre estos escritorios si los encuentra parados.

Para poder generar un server a partir de una plantilla se tiene que ir al panel de "Administración".

Se pulsa el botón ![](./template_server.es.images/template_server1.png)

![](./template_server.es.images/template_server2.png)

Una vez allí se va a "Templates" bajo el apartado de "Manager".

Se pulsa el botón ![](./template_server.images/template_server3.png)

![](./template_server.images/template_server4.png)

Se pulsa el botón ![](./template_server.images/template_server5.png) de la plantilla que se quiera convertir en server.

![](./template_server.images/template_server6.png)

Se pulsa el botón ![](./template_server.images/template_server7.png)

![](./template_server.images/template_server8.png)

Y aparecerá una ventana de diálogo. Se tiene que marcar la casilla "Set it as server"

![](./template_server.images/template_server9.png)