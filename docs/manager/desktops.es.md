# Escritorios

Para ir a la sección "Desktops", se pulsa el botón ![Administration](../admin/users.ca.images/users1.png)

![pantalla de la página inicial](../admin/users.ca.images/users2.png)

Y en la sección "Desktops"

![barra lateral de administración](./domains.images/domains2.png)

En esta sección se pueden ver todos los escritorios que se han creado de todas las categorías.

Al hacer clic en el icono ![[+]](./domains.images/domains5.png), se mostrará más información sobre el escritorio.

* Status detailed info: aquí se muestra el estado del escritorio. En caso de escritorio fallado, se mostrará un mensaje de error que puede ayudar a identificar el problema

* Description: descripción del escritorio

* Hardware: como su nombre indica, es el hardware que se ha asignado a este escritorio

* Storage: indica el UUID del disco asociado y su uso

* Media: indica que los medios asociados al escritorio, si hay alguno

![Detalles del escritorio](./domains.images/domains6.png)

### Acciones globales

En la parte superior de la tabla tenemos un desplegable con varias acciones. Estas acciones se aplicarán a todos los escritorios seleccionados.

![Desplegable de acciones globales](./domains.images/domains54.png)

Para seleccionar un escritorio, simplemente se tiene que hacer clic en la fila de la tabla. Los escritorios seleccionados aparecerán como marcados en la casilla de "selected" al final de la fila. Aunque se navegue a la página siguiente, los escritorios seleccionados permanecerán marcados.

![Checkboxes de selección](./domains.images/domains55.png)

Alternativamente, si no se selecciona ningún escritorio, se pueden utilizar los filtros de arriba o debajo de la tabla para filtrarlos.

!!! Warning Aviso
    Si no se selecciona ningún escritorio o aplicáis ningún filtro, todos los escritorios se seleccionarán por defecto.

Una vez se hayan seleccionado los escritorios, se elige una acción del desplegable para realizarla.

<!-- * Soft toggle started/shutting-down state:

* Toggle started/stopped state:

* Soft shut down:

* Shut down:

* Force Failed state:

* Delete:

* Start Paused (check status):

* Remove forced hypervisor:

* Remove favourite hypervisor: -->

### Edición en bloc

Se pueden seleccionar varios escritorios haciendo clic en las filas de la tabla. De manera alternativa, se pueden filtrar los escritorios sin hacer clic en las filas y todos los escritorios que se muestran en la tabla contarán como seleccionados, igual que se hace con Global Actions.

Al hacer clic en el botón ![Bulk Edit Desktops](./domains.images/domains40.png) aparecerá una ventana:

![(Ventana de actualizar múltiples escritorios a la vez)](./domains.images/domains41.png)

* Desktops to edit: Indica todos los escritorios que se actualizarán con los datos del formulario. En caso de haber muchos escritorios, se puede ir desplazando por la lista para seleccionarlos."

* Desktops viewers: si está marcada, aparecerá una sección nueva. Los visores de los escritorios se actualizarán según esté seleccionado.

![Update viewers section](./domains.images/domains42.png)

Si está marcada la opción "Update RDP credentials", también se cambiará el nombre de usuario y la contraseña utilizados para las sesiones RDP.

![(Update RDP credentials)](./domains.images/domains43.png)

* Hardware: los datos solo se cambiarán en los campos donde el valor seleccionado es diferente a "--"

Si está marcada la opción "Update network", aparecerá una sección nueva. Las interfaces de los escritorios seleccionadas se actualizarán, usando la tecla CTRL mientras se hace clic a las opciones para seleccionar más de una interfaz a la vez.

![(Update network)](./domains.images/domains44.png)

* Bookable resources: Los reservables se actualizarán cuando el valor seleccionado sea diferente a "--"

Para aplicar todos los cambios y actualizar los escritorios seleccionados, se tiene que hacer clic al botón ![(Modify desktops)](./domains.images/domains45.png)

### Crear en bloc

Al hacer clic en ![Bulk Add Desktops](./domains.images/domains56.png) aparecerá una ventana

![(Modal de Bulk Add)](./domains.images/domains57.png)

Este formulario permite crear varios escritorios simultáneamente. Para crearlos, simplemente se tiene que rellenar el formulario especificando un nombre para los escritorios nuevos y seleccionando la plantilla sobre la que se basarán.

A la sección "Allowed", se pueden seleccionar los usuarios para los cuales se quieren crear los escritorios eligiendo entre grupos o nombres de usuario. Se creará un escritorio separado para cada usuario seleccionado.

### Editar

Al hacer clic al botón ![Edit](./domains.images/domains51.png) aparecerá una ventana. En esta ventana se pueden editar los parámetros del escritorio.

### XML

Al hacer clic al botón ![XML](./domains.images/domains52.png) aparecerá una ventana donde se puede modificar el fichero de configuración de la máquina virtual [KVM/QEMU XML](https://libvirt.org/formatdomain.html).
