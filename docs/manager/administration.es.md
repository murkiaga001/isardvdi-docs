# Administración

!!! info "Roles con acceso"

    Solo los **gestores** (managers) y los **administradores** tienen acceso a esta característica.
    Los gestores están restringidos a su propia categoría.

La interfaz web de administración permite a los gestores y administradores configurar y gestionar funciones avanzadas y gestionar todos los usuarios del sistema, escritorios, plantillas y medios.

Los usuarios de roles de administración también tienen acceso a descargas IsardVDI, gestión de hipervisors y configuración del sistema.

En el [**Manual Administrador**](../../admin/index.es/) encontraréis estas prestaciones.
