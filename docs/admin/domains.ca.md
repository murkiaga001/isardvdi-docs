# Dominis

!!! info "Rols amb accés"

    Només els **administradors** tenen accés a totes aquestes característiques.

Per a anar a la secció de "Domains", es prem el botó ![](./users.ca.images/users1.png)

![](./users.ca.images/users2.png)

I es prem el menú desplegable ![](./domains.images/domains1.png)

![](./domains.images/domains2.png)


## Escriptoris

En aquesta secció es poden veure tots els escriptoris que s'hagin creat de totes les categories.

![](./domains.images/domains4.png)

Si es prem la icona ![[+]](./domains.images/domains5.png), es pot veure més informació de l'escriptori.

* Status detailed info: aquí es mostra l'estat de l'escriptori. En cas d'escriptori fallat, es mostrarà un missatge d'error que pot ajudar a identificar el problema

* Description: descripció de l'escriptori

* Hardware: com el seu nom indica, és el maquinari que s'ha assignat a aquest escriptori

* Template tree: una representació visual que mostra la jerarquia d'escriptoris i la plantilla d'on es deriven aquests escriptoris i les seves dependències. Això és útil per comprendre l'origen d'un escriptori en particular.

* Storage: indica l'UUID del disc associat i el seu ús

* Media: indica que els mitjans associats a l'escriptori, si n'hi ha cap


![](./domains.images/domains6.png)

### Global actions

A la part superior de la taula tenim un desplegable amb diverses accions. Aquestes accions s'aplicaran a tots els escriptoris seleccionats.

![Desplegable d'accions globals](./domains.images/domains54.png)

Per a seleccionar un escriptori, simplement s'ha de fer clic a la fila de la taula. Els escriptoris seleccionats apareixeran com a marcats a la casella de "selected" al final de la fila. Encara que es navegui a la pàgina següent, els escriptoris seleccionats romandran marcats.

![Checkboxes de selecció](./domains.images/domains55.png)

Alternativament, si no se selecciona cap escriptori, es poden fer servir els filtres de dalt o de sota de la taula per a filtrar-los.

!!! Warning Avís
    Si no se selecciona cap escriptori o apliqueu cap filtre, tots els escriptoris se seleccionaran per defecte.

Un cop s'hagin seleccionat els escriptoris, es tria una acció del desplegable per a realitzar-la.

<!-- * Soft toggle started/shutting-down state:

* Toggle started/stopped state:

* Soft shut down:

* Shut down:

* Force Failed state:

* Delete:

* Start Paused (check status):

* Remove forced hypervisor:

* Remove favourite hypervisor: -->### Global actions

A la part superior de la taula tenim un desplegable amb diverses accions. Aquestes accions s'aplicaran a tots els escriptoris seleccionats.

![Desplegable d'accions globals](./domains.images/domains54.png)

Per a seleccionar un escriptori, simplement s'ha de fer clic a la fila de la taula. Els escriptoris seleccionats apareixeran com a marcats a la casella de "selected" al final de la fila. Encara que es navegui a la pàgina següent, els escriptoris seleccionats romandran marcats.

![Checkboxes de selecció](./domains.images/domains55.png)

Alternativament, si no se selecciona cap escriptori, es poden fer servir els filtres de dalt o de sota de la taula per a filtrar-los.

!!! Warning Avís
    Si no se selecciona cap escriptori o apliqueu cap filtre, tots els escriptoris se seleccionaran per defecte.

Un cop s'hagin seleccionat els escriptoris, es tria una acció del desplegable per a realitzar-la.

<!-- * Soft toggle started/shutting-down state:

* Toggle started/stopped state:

* Soft shut down:

* Shut down:

* Force Failed state:

* Delete:

* Start Paused (check status):

* Remove forced hypervisor:

* Remove favourite hypervisor: -->

### Bulk Edit Desktops

Es poden seleccionar diversos escriptoris fent clic a les files de la taula. De manera alternativa, es poden filtrar els escriptoris sense fer clic a les files i tots els escriptoris que es mostren a la taula comptaran com a seleccionats, igual que es fa amb Global Actions.

Al fer clic al botó ![Bulk Edit Desktops](./domains.images/domains40.png) apareixerà una finestra:

![(Finestra d'actualitzar múltiples escriptoris alhora)](./domains.images/domains41.png)

* Desktops to edit: Indica tots els escriptoris que s'actualitzaran amb les dades del formulari. Si hi ha molts escriptoris, es pot anar desplaçant per la llista.

* Desktops viewers: si està marcada, apareixerà una secció nova. Els visors dels escriptoris s'actualitzaran segons estigui seleccionat.

![Update viewers section](./domains.images/domains42.png)

Si està marcada l'opció "Update RDP credentials", també es canviarà el nom d'usuari i la contrasenya utilitzats per les sessions RDP.

![(Update RDP credentials)](./domains.images/domains43.png)

* Hardware: les dades només es canviaran en els camps on el valor seleccionat és diferent de "--"

Si està marcada l'opció "Update network", apareixerà una secció nova. Les interfícies dels escriptoris seleccionades s'actualitzaran, fent servir la tecla CTRL mentre es fa clic a les opcions per a seleccionar més d'una interfície alhora.

![(Update network)](./domains.images/domains44.png)

* Bookable resources: Els reservables s'actualitzaran quan el valor seleccionat sigui diferent de "--"

Per aplicar tots els canvis i actualitzar els escriptoris seleccionats, s'ha de fer clic al botó ![(Modify desktops)](./domains.images/domains45.png)

### Bulk Add Desktops

Al fer clic a ![Bulk Add Desktops](./domains.images/domains56.png) apareixerà una finestra

![(Modal de Bulk Add)](./domains.images/domains57.png)

Aquest formulari permet crear diversos escriptoris simultàniament. Per crear-los, simplement s'ha d'omplir el formulari especificant un nom pels escriptoris nous i seleccionant la plantilla en què es basaran.

A la secció "Allowed", es poden seleccionar els usuaris pels quals es volen crear els escriptoris triant entre rols, categories, grups o noms d'usuari. Es crearà un escriptori separat per cada usuari seleccionat.

## Plantilles

En aquesta secció es poden veure totes les plantilles que s'hagin creat de totes les categories.

![](./domains.images/domains7.png)

Si es fa clic al botó ![](./domains.images/domains49.png) situat a la cantonada superior dreta, les plantilles que no estan habilitades (visibles pels usuaris amb els que es comparteix) no apareixeran a la taula. Quan torneu a fer clic al botó ![](./domains.images/domains50.png) es mostraran de nou.

Si es prem la icona ![](./domains.images/domains5.png), es pot veure més informació de la plantilla.

* Hardware: com el seu nom indica, és el maquinari que se li ha assignat a aquesta plantilla

* Allows: indica a qui se li ha donat permisos per utilitzar la plantilla

* Storage: indica l'UUID del disc associat a la plantilla i el seu ús

* Media: indica els mitjans associats a la plantilla, si hi ha cap

![](./domains.images/domains8.png)

### Duplicat

Aquesta opció permet duplicar la plantilla a la base de dades, mentre que es manté el disc original.

La duplicació de plantilles es pot realitzar diverses vegades i és útil a alguns casos, com ara:

- Es vol compartir la plantilla en altres categories. Es podria duplicar la plantilla i assignar-la a un altre usuari en una categoria diferent amb permisos de compartició separats per la còpia nova i mantenir la versió original

L'eliminació d'una plantilla duplicada no eliminarà la plantilla original.

En duplicar una plantilla, es pot personalitzar la còpia nova:

- Nom de la nova plantilla
- Descripció de la nova plantilla
- Usuari assignat com a nou propietari de la plantilla
- Habilitar o deshabilitar la visibilitat de la plantilla als usuaris compartits
- Seleccionar els usuaris amb qui es vol compartir la nova plantilla

![](./domains.images/domains46.png)

#### Esborrat

Aquesta opció eliminarà el domini i el seu disc.

En eliminar una plantilla, apareixerà un modal que mostrarà tots els escriptoris creats a partir d'ella i qualsevol plantilla duplicada. Eliminar la plantilla comportarà l'eliminació de tots els dominis associats i els seus discs.

![](./domains.images/domains47.png)

Tot i això, si la plantilla a eliminar és una duplicada d'una altra plantilla, no cal eliminar-les totes. Només s'haurien d'eliminar si també s'elimina la plantilla d'origen.

![](./domains.images/domains48.png)


#### Editar

Al fer clic al botó ![Edit](./domains.images/domains51.png) apareixerà una finestra. En aquesta finestra es poden editar els paràmetres de l'escriptori.

#### XML

Al fer clic al botó ![XML](./domains.images/domains52.png) apareixerà una finestra on es pot modificar el fitxer de configuració de la màquina virtual [KVM/QEMU XML](https://libvirt.org/formatdomain.html).


## Mitjans

En aquesta secció es poden veure tots els arxius mitjana que s'hagin pujat de totes les categories.

![](./domains.images/domains9.png)


## Recursos

En aquesta secció es poden veure tots els recursos que té la instal·lació, aquests sortiran en crear un escriptori/plantilla.

![](./domains.images/domains10.png)


### Grafics

En aquesta secció es pot afegir diferents tipus de visors amb els quals es poden veure un escriptori.

Per a afegir un visor nou, es prem el botó ![](./domains.images/domains11.png)

![](./domains.images/domains12.png)

I s'emplena el formulari

![](./domains.images/domains13.png)


### Vídeos

En aquesta secció es poden afegir diferents formats de vídeo.

Per a afegir un nou format, es prem el botó ![](./domains.images/domains11.png)

![](./domains.images/domains14.png)

I s'emplena el formulari

![](./domains.images/domains15.png)

(heads -> cuantos monitores quieres)

### Interfícies

En aquesta secció es poden afegir xarxes privades als escriptoris.

Per a afegir una xarxa, es prem el botó ![](./domains.images/domains11.png)

![](./domains.images/domains16.png)

I s'emplena el formulari

* Type: Bridge , Network, OpenVSwitch, Personal

    - Bridge: Enllaça amb un pont cap a una xarxa del servidor. En el servidor es pot tenir interfícies que enllacin per exemple amb vlans cap a un troncal de la teva xarxa, i es pot mapear aquestes interfícies en isard i connectar-les als escriptoris.

    Per a poder mapear la interfície dins del hipervisor, en l'arxiu de isardvdi.conf s'ha de modificar aquesta línia:

    ```
    # ------ Trunk port & vlans --------------------------------------------------
    ## Uncomment to map host interface name inside hypervisor container.
    ## If static vlans are commented then hypervisor will initiate an 
    ## auto-discovery process. The discovery process will last for 260
    ## seconds and this will delay the hypervisor from being available.
    ## So it is recommended to set also the static vlans.
    ## Note: It will create VlanXXX automatically on webapp. You need to
    ## assign who is allowed to use this VlanXXX interfaces.
    #HYPERVISOR_HOST_TRUNK_INTERFACE=

    ## This environment variable depends on previous one. When setting
    ## vlans number comma separated it will disable auto-discovery and
    ## fix this as forced vlans inside hypervisor.
    #HYPERVISOR_STATIC_VLANS=
    ```


* Model: rtl8931, virtio, e1000

    - És més eficient utilitzar virtio (que és una interfície paravirtualizada), mentre que les e1000 o rtl són simulacions de targetes, i va més lent encara que és més compatible amb sistemes operatius antics i no es necessita instal·lar drivers en el cas de windows.

    **Si es té un sistema operatiu modern, amb el virtio funciona, sinó amb els altres que són interfícies. Windows antics: rtl, e1000**


* QoS: limit up and down to 1 Mbps, unlimited

![](./domains.images/domains17.png)


### Mètodes d'arrencada

En aquesta secció s'indiquen les diferents maneres d'arrencada d'un escriptori.

**Si no es té permisos no es pot seleccionar una iso d'arrencada i no es deixa per defecte que els usuaris es puguin mapear isos.**

Per a donar permisos es prem en la icona ![](./domains.images/domains25.png)

![](./domains.images/domains18.png)


### QoS de Xarxa

En aquesta secció s'indiquen les limitacions que es poden posar a les xarxes.

Per a afegir una limitació, es prem el botó ![](./domains.images/domains11.png)

![](./domains.images/domains19.png)

I s'emplena el formulari

![](./domains.images/domains20.png)


### QoS de disc

En aquesta secció s'indiquen les limitacions que es poden posar als discos.

Per a afegir un límit de disc, es prem el botó ![](./domains.images/domains11.png)

![](./domains.images/domains21.png)

I s'emplena el formulari

![](./domains.images/domains22.png)


### VPNs remotes

En aquesta secció es poden afegir xarxes remotes als escriptoris.

Per a afegir una xarxa remota, es prem el botó ![](./domains.images/domains11.png)

![](./domains.images/domains23.png)

I s'emplena el formulari

![](./domains.images/domains24.png)


## Bookables



### Priority



### Resources