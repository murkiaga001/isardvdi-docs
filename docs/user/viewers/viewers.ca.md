# Visors

## Descàrrega directa de programari

<table>
  <thead>
    <tr>
      <th>Sistema operatiu</th>
      <th>Visor SPICE</th>
      <th>Visor RDP</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td> Windows</td>
      <td>
        <a href="https://releases.pagure.org/virt-viewer/virt-viewer-x64-11.0-1.0.msi">virt-viewer 11</a>
      </td>
      <td>No fa falta instal·lar res</td>
    </tr>
    <tr>
      <td>Linux</td>
      <td>
        <code>sudo apt install virt-viewer / sudo dnf install virt-viewer</code>
      </td>
      <td>
        <code>sudo apt install remmina / sudo dnf install remmina</code>
      </td>
    </tr>
    <tr>
      <td>Mac OS</td>
      <td>Pots seguir aquesta <a href="https://gist.github.com/tomdaley92/789688fc68e77477d468f7b9e59af51c">guia d'instal·lació</a></td>
      <td><a href="https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12">Microsoft Remote Desktop</a></td>
    </tr>
    <tr>
      <td>Android</td>
      <td><a href="https://play.google.com/store/apps/details?id=com.iiordanov.freeaSPICE">aSPICE</a></td>
      <td><a href="https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx">Remote Desktop</a></td>
    </tr>
    <tr>
      <td>Apple iOS</td>
      <td><a href="https://apps.apple.com/gb/app/aspice-pro/id1560593107">aSPICE Pro</a></td>
      <td><a href="https://apps.apple.com/us/app/remote-desktop-mobile/id714464092">Remote Desktop Mobile</a></td>
    </tr>
  </tbody>
</table>



## SPICE

![](viewers.ca.images/visor_spice_1.png){width="30%"}


### Descripció

SPICE és un protocol de comunicació per a entorns virtuals que permet accedir a la senyal de vídeo, ratolí i teclat cap a l'escriptori com si es tingués connexió a la pantalla, ratolí i teclat d'un equip real.

* **Avantatges**:
    * És el propi motor de màquines virtuals que utilitza Isard (Qemu-KVM) el que permet l'accés, independentment del sistema operatiu que estigui en execució.
    * Es pot utilitzar aquest protocol per veure tota la seqüència, des del principi, de l'arrencada de l'escriptori.
    * No cal instal·lar cap component al sistema operatiu de l'escriptori virtual per poder crear una interacció.
    * És un protocol que optimitza l'amplada de banda de vídeo utilitzada comprimint la senyal i enviant només les zones que varien d'un frame a un altre.

* **Inconvenients**:
    * El client no ve instal·lat per defecte en cap sistema operatiu, la instal·lació és molt senzilla i la pot fer qualsevol usuari, però en entorns corporatius o educatius, les restriccions de permisos poden dificultar la instal·lació.
    * La instal·lació a Windows requereix d'un programa addicional per poder redirigir els ports USB.
    * La connexió es fa a través d'un proxy HTTP utilitzant un mètode "CONNECT". Aquest mètode, en alguns casos, és filtrat per algun firewall o proxy intermedi.

A destacar d'aquest visor:

* Baixa latència
* Àudio integrat
* Opció de connexió de dispositius


### Com utilitzar el visor SPICE

Després de realitzar la [descàrrega del programari](#descarrega-directa-de-programari) necessari, en l'arrencada d'un escriptori, es selecciona **Visor SPICE** i s'obre l'arxiu descarregat. Un cop obert, es podrà accedir a l'escriptori.

![](viewers.ca.images/visor_spice_2.png){width="80%"}


### Escanejar dispositius USB

!!! Info
    **PER AMFITRIONS WINDOWS**
    
    Si es té instal·lat **virt-viewer 11**, s'ha de realitzar un d'aquests pasos per actviar l'escaneig de USB:

    - Opció 1
        - Desinstal·lar **virt-viewer 11** i instal·lar aquesta versió de **[virt-viewer 7](https://releases.pagure.org/virt-viewer/virt-viewer-x64-7.0.msi)**
        - Instal·lar paquet **[UsbDk](https://github.com/daynix/UsbDk/releases/download/v1.00-22/UsbDk_1.0.22_x64.msi)**

    - Opció 2
        - Instal·lar paquet **[UsbDk](https://github.com/daynix/UsbDk/releases/download/v1.00-22/UsbDk_1.0.22_x64.msi)**
        - Canviar el fitxer "**libusb-1.0.dll**" a la ruta "**C:\Archivos de programa\VirtViewer v11.0-256**" per **[aquest altre](https://nextcloud.isardvdi.com/s/jSngRESwbYZHRks)**


#### virt-viewer 7

S'obre el visor SPICE de l'escriptori i se seleccionen les opcions **Fitxer >> Selecció del dispositiu USB** i es tria el dispositiu a escanejar.

![](viewers.es.images/visor_spice_4.png){width="70%"}

![](viewers.es.images/visor_spice_5.png){width="70%"}


#### virt-viewer 11 (Windows)

S'obre el visor SPICE de l'escriptori, es prem el segon botó de la finestra del visor i es tria el dispositiu a escanejar.

![](viewers.ca.images/visor_spice_3.png){width="80%"}

![](viewers.ca.images/visor_spice_4.png){width="70%"}


## VNC al navegador

![](viewers.ca.images/visor_al_navegador_3.png){width="30%"}


### Descripció

NoVNC és un protocol que treballa al mateix nivell que SPICE, però com que és el protocol més antic, la senyal de vídeo no és massa òptima. És el protocol que s'utilitza en el visor integrat al navegador. A destacar d'aquest visor:

* Funciona amb qualsevol navegador web modern
* Latència mitjana
* Àudio no disponible
* Opció de connexió de dispositius no disponible


### Com utilitzar el visor al navegador

Es selecciona l'opció **Visor VNC al navegador** dels visors disponibles d'un escriptori arrencat.

!!! Info
    <span style="font-size: medium">Cal tenir en compte que l'opció d'obrir finestres emergents pot estar bloquejada en el navegador. Si això passa, el visor al navegador no s'obrirà. 
    En aquest cas, per exemple, per al Firefox es pot solucionar de la següent manera:</span>

    <span style="font-size: medium">Apareixerà un missatge a la pestanya on actualitzar aquesta configuració:</span>

    ![](viewers.ca.images/visor_al_navegador_1.png)

    ![](viewers.ca.images/visor_al_navegador_2.png){width="50%"}

Automàticament s'obrirà el visor de l'escriptori en una nova pestanya.

![](viewers.es.images/visor_en_el_navegador_2.png){width="80%"}


## RDP

![](viewers.ca.images/visor_rdp_2.png){width="22%"}
![](viewers.ca.images/visor_rdp_4.png){width="28%"}
![](viewers.ca.images/visor_rdp_3.png){width="32%"}


### Descripció

#### RDP Natiu

RDP és el protocol que s'utilitza per defecte per connectar-se de forma remota a un equip amb el sistema operatiu Windows. Aquests visors no estan disponibles des de l'arrencada de l'escriptori, i cal esperar que durant el procés, l'escriptori obtingui una adreça IP.

* **Avantatges**:
    * Millor experiència d'usuari si el sistema operatiu de l'escriptori virtual és Windows, i també si ho és la màquina amfitriona.
    * Dit això, no cal instal·lar programari addicional als equips amb Windows, ja que el Client d'Escriptori Remot ve integrat per defecte en el sistema operatiu.
    * És necessari per a una bona experiència d'usuari utilitzar vGPUs de NVIDIA sobre sistemes operatius Windows.

* **Inconvenients**:
    * Si hi ha algun problema en l'arrencada del sistema operatiu, no s'accedeix a la senyal de pantalla (es soluciona connectant-se a través dels visors SPICE o al navegador).

A destacar d'aquests visors:

- Baixa latència (millor experiència en escriptoris amb S.O. Windows)
- Àudio integrat
- Opció de connexió de dispositius
- Millor experiència en utilitzar aplicacions de disseny que requereixen vGPU
- Escriptoris amb vGPU només funcionen amb visors RDP


#### RDP al navegador

IsardVDI utilitza el servidor *Guacamole* que permet que els clients RDP en HTML5 (qualsevol navegador actual) es connectin als guest de Windows d'IsardVDI a través del port HTTPS predeterminat. A més, compta amb àudio a través del navegador.

* Funciona amb qualsevol navegador modern
* Baixa latència
* Àudio integrat
* Opció de connexió de dispositius no disponible


### Com utilitzar els visors RDP i RDP VPN

Com s'ha comentat anteriorment, **el visor RDP necessita una adreça IP** a la qual connectar-se per establir la connexió per RDP, és per això que quan s'arrenca un escriptori, observem un parpelleig on apareixen els enllaços als visors RDP com a no seleccionables.

Un cop l'escriptori ha obtingut una adreça IP, se l'informa a l'usuari a través de la interfície i s'activa l'accés als visors RDP, RDP al navegador i RDP VPN:

![](viewers.ca.images/visor_rdp_5.png){width="25%"}
![](viewers.ca.images/visor_rdp_6.png){width="25%"}

!!! Warning
    <span style="font-size: medium">L'escriptori ha de tenir activada la interfície ***Wireguard VPN*** perquè els visors RDP puguin connectar-se.</span>

!!! Info
    <span style="font-size: medium">El visor **RDP VPN** s'utilitza exactament igual que el visor RDP natiu, amb l'excepció que l'usuari ha d'establir la **connexió VPN** entre IsardVDI i el seu equip personal. Consulteu **[com utilitzar la VPN](https://isard.gitlab.io/isardvdi-docs/user/vpn.ca/)** del manual.</span>


#### Amb amfitrió Windows

En primer lloc, generalment Windows ja té integrat el Client d'Escriptori Remot en els nous sistemes.

En seleccionar els visors RDP o RDP VPN, es descarrega un fitxer amb extensió *.rdp*, el qual conté la informació per poder connectar-se a l'escriptori:

![](viewers.es.images/visor_rdp_10.png){width="40%"}

La primera vegada que es realitza la connexió RDP a l'amfitrió, aquest informa amb una alerta de seguretat, la qual es pot evitar marcant la casella "**No tornis a preguntar-me sobre connexions a aquest equip**" per al futur.

![](viewers.es.images/visor_rdp_11.png){width="50%"}

Les credencials per accedir als escriptoris que Isard ofereix per defecte són:

* usuari: **isard**
* contrasenya: **pirineus**

!!! Warning
    <span style="font-size: medium">Aquest usuari i contrasenya corresponen a les credencials de l'usuari creat al S.O. de l'escriptori virtual al qual es vol accedir, perquè aquest pugui iniciar sessió a l'equip.</span>
    
    <span style="font-size: medium">Si es té un escriptori amb un usuari personalitzat, cal **[modificar aquestes credencials a l'escriptori des de la interfície d'Isard](../../edit_desktop.ca/#login-rdp)**.</span>

![](viewers.es.images/visor_rdp_12.png){width="40%"}

En accedir a l'escriptori, ens demana confirmació per acceptar el certificat:

![](viewers.es.images/visor_rdp_13.png){width="45%"}

Finalment, s'obre el client i podem interactuar amb l'escriptori.


#### Amb amfitrió Linux

Després de realitzar la [descàrrega del programa Remmina](#descarrega-directa-de-programari) mitjançant la línia de comandes, en l'arrencada d'un escriptori, es seleccionen les opcions **Visor RDP** o **Visor RDP VPN** i s'obre l'arxiu descarregat amb el programa **Remmina** recentment instal·lat. Un cop obert, es podrà accedir a l'escriptori.

![](viewers.ca.images/visor_rdp_9.png){width="80%"}

![](viewers.ca.images/visor_rdp_10.png){width="80%"}


### Escanejar dispositius USB

#### Amb amfitrió Windows

Del fitxer ***.rdp*** que es descarrega en seleccionar **Visor RDP** o **Visor RDP VPN** d'un escriptori en marxa, es modifica amb el botó dret en la ruta on es trobi.

![](viewers.es.images/visor_rdp_10.png){width="40%"}
![](viewers.es.images/visor_rdp_14.png){width="45%"}


**Recursos locals >> Més >> Seleccionar la unitat USB >> Connectar**

![](viewers.es.images/visor_rdp_15.png){width="40%"}
![](viewers.es.images/visor_rdp_16.png){width="40%"}

![](viewers.es.images/visor_rdp_17.png){width="40%"}
![](viewers.es.images/visor_rdp_18.png){width="40%"}

En accedir al visor RDP, apareix el dispositiu en el llistat de discs de l'escriptori.

![](viewers.es.images/visor_rdp_19.png){width="80%"}


#### Amb amfitrió Linux

De moment, **no és possible escanejar dispositius USB mitjançant el client Remmina** per a sistemes operatius Linux.


### Com utilitzar el visor RDP al navegador

De la mateixa manera que amb el **[visor VNC al navegador](#vnc-al-navegador)**, s'obrirà el visor en una nova pestanya del navegador, amb accés directe a l'escriptori.

!!! Warning
    <span style="font-size: medium">L'escriptori ha d'estar actiu amb la interfície ***Wireguard VPN*** perquè els visors RDP puguin connectar-se.</span>


## Activar RDP

Així com ha d'estar activat a l'equip amfitrió, com s'ha explicat anteriorment en aquest manual, el protocol RDP també ha d'estar activat dins del sistema operatiu dels propis escriptoris virtuals.

!!! Tip
    <span style="font-size: medium">Les **plantilles predissenyades que ofereix IsardVDI** ja tenen el sistema operatiu **preparat per a les connexions Wireguard** amb els visors RDP, per la qual cosa aquest apartat del manual **no és necessari** si l'usuari crea escriptoris basats en aquestes plantilles.</span>

    <span style="font-size: medium">En cas contrari, si l'usuari ha creat un **escritori personalitzat pel seu compte**, aquí s'explica com configurar el seu sistema per poder accedir mitjançant els visors RDP.</span>

### Per a escriptoris amb Windows

**1- Activar RDP**

![](viewers.ca.images/visor_rdp_7.png){width="70%"}

**2- Deshabilitar l'autenticació de xarxa**

![](viewers.ca.images/visor_rdp_8.png){width="70%"}

![](viewers.es.images/guest_rdp_cfg2.png){width="40%"}

L'escriptori està preparat.


### Per a escriptoris amb Linux

Es pot seguir **[aquest apartat del manual per a sistemes operatius Ubuntu](../../../guests/ubuntu_22.04/desktop/activate_rdp/configuration.ca)**.


## Detalls tècnics

A la VDI (Infraestructura de l'Escriptori Virtual), els visors són importants perquè permeten l'accés remot als escriptoris virtuals. La VDI permet als usuaris accedir a un escriptori virtual des de qualsevol dispositiu amb connexió a Internet, i els visors proporcionen la interfície perquè l'usuari interactuï amb aquest escriptori virtual.

Els visors solen oferir característiques com protocols de visualització, compressió, redirecció multimèdia, redirecció de dispositius USB i autenticació. Permeten als usuaris accedir a l'entorn de l'escriptori virtual, utilitzar aplicacions i accedir a dades des d'una ubicació remota com si estiguessin físicament asseguts a l'escriptori.

Els visors també juguen un paper fonamental en garantir que l'experiència de l'escriptori virtual sigui el més semblant possible a l'experiència de l'equip amfitrió. Han de ser capaços de gestionar gràfics d'alta resolució, contingut multimèdia i altres tipus de dades sense causar retardaments ni interrupcions en l'experiència de l'usuari.

En resum, els visors són essencials en la VDI perquè proporcionen els mitjans perquè els usuaris accedeixin i interactuïn amb els escriptoris virtuals, i juguen un paper fonamental en garantir que l'experiència de l'escriptori virtual sigui fluïda i eficient.

També pots activar l'opció de **visor directe** que et proporcionarà un enllaç directe per connectar-te al teu escriptori sense necessitat d'autenticar-te al sistema.


### Taula resum sobre les característiques dels visors

<table>
  <thead>
    <tr>
      <th>IsardVDI Viewer</th>
      <th>SPICE Viewer</th>
      <th>Browser Viewer</th>
      <th>RDP Viewer</th>
      <th>RDP Browser viewer</th>
      <th>RDP VPN viewer</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><strong>Protocol</strong></td>
      <td>SPICE</td>
      <td>VNC-SPICE</td>
      <td>RDP</td>
      <td>RDP</td>
      <td>RDP</td>
    </tr>
    <tr>
      <td><strong>HTML5 web client</strong></td>
      <td>-</td>
      <td>NoVNC</td>
      <td>-</td>
      <td>Guacamole</td>
      <td>-</td>
    </tr>
    <tr>
      <td><strong>Windows viewer</strong></td>
      <td>Remote-viewer<br><a href="https://releases.pagure.org/virt-viewer/virt-viewer-x64-11.0-1.0.msi">virt-viewer 11 installer</a></td>
      <td>Web Browser</td>
      <td>Remote Desktop Viewer (windows)</td>
      <td>Web Browser</td>
      <td>-</td>
    </tr>
    <tr>
      <td><strong>Linux viewer</strong></td>
      <td>sudo apt install virt-viewer<br>sudo dnf install remote-viewer</td>
      <td>Web Browser</td>
      <td>Remmina</td>
      <td>Web Browser</td>
      <td>Remmina</td>
    </tr>
    <tr>
      <td><strong>Mac Viewer</strong></td>
      <td>You can follow the <a href="https://gist.github.com/tomdaley92/789688fc68e77477d468f7b9e59af51c">installation guide</a></td>
      <td>Web Browser</td>
      <td><a href="https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12">Apple Store</a></td>
      <td>Web Browser</td>
      <td><a href="https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12">Apple Store</a></td>
    </tr>
    <tr>
      <td><strong>Android Viewer</strong></td>
      <td><a href="https://play.google.com/store/apps/details?id=com.iiordanov.freeaSPICE">Free version</a> on the Play Store and also a <a href="https://play.google.com/store/apps/details?id=com.iiordanov.aSPICE">paid version</a> with more features.</td>
      <td>Web Browser</td>
      <td><a href="https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx">remote desktop</a></td>
      <td>Web Browser</td>
      <td><a href="https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx">remote desktop</a></td>
    </tr>
    <tr>
      <td><strong>Apple iOS Viewer</strong></td>
      <td><a href="https://apps.apple.com/gb/app/aspice-pro/id1560593107">Paid version</a> on Apple Store</td>
      <td>-</td>
      <td><a href="https://apps.apple.com/us/app/remote-desktop-mobile/id714464092">remote desktop</a></td>
      <td>-</td>
      <td><a href="https://apps.apple.com/us/app/remote-desktop-mobile/id714464092">remote desktop</a></td>
    </tr>
    <tr>
      <td><strong>Download File Extension</strong></td>
      <td>*.vv</td>
      <td>Web Browser</td>
      <td>*.rdp</td>
      <td>-</td>
      <td>*.rdp</td>
    </tr>
    <tr>
      <td><strong>Copy / Paste real desktop to virtual desktop</strong></td>
      <td>✓</td>
      <td>✗</td>
      <td>✓</td>
      <td>✓</td>
      <td>✓</td>
    </tr>
    <tr>
      <td><strong>Wireguard client required to access user VPN</strong></td>
      <td>✗</td>
      <td>✗</td>
      <td>✗</td>
      <td>✗</td>
      <td>✓</td>
    </tr>
    <tr>
      <td><strong>USB Redirection</strong></td>
      <td>✓ <br>(with windows you need to install usb)</td>
      <td>✗</td>
      <td>✓</td>
      <td>✗</td>
      <td>✓</td>
    </tr>
    <tr>
      <td><strong>Audio</strong></td>
      <td>✓</td>
      <td>✗</td>
      <td>✓</td>
      <td>✗</td>
      <td>✓</td>
    </tr>
    <tr>
      <td><strong>Adapt desktop screen size to viewer</strong></td>
      <td>✓</td>
      <td>✓</td>
      <td>✓</td>
      <td>✓</td>
      <td>✓</td>
    </tr>
    <tr>
      <td><strong>You can see how the desktop boots</strong></td>
      <td>✓</td>
      <td>✓</td>
      <td>✗</td>
      <td>✗</td>
      <td>✗</td>
    </tr>
    <tr>
      <td><strong>You must have IP address and RDP service on your virtual desktop</strong></td>
      <td>✗</td>
      <td>✗</td>
      <td>✓</td>
      <td>✓</td>
      <td>✓</td>
    </tr>
  </tbody>
</table>


### Taula resum sobre compatibilitat en sistemes operatius

<table>
  <thead>
    <tr>
      <th>VDI Viewer</th>
      <th>Supported OS</th>
      <th>Protocols</th>
      <th>Multimedia Redirection</th>
      <th>USB Redirection</th>
      <th>Video Compression</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Microsoft Remote Desktop</td>
      <td>Windows, Mac, iOS, Android</td>
      <td>RDP</td>
      <td>✓</td>
      <td>✓</td>
      <td>RemoteFX</td>
    </tr>
    <tr>
      <td>Spice</td>
      <td>Windows, Linux</td>
      <td>SPICE</td>
      <td>✓</td>
      <td>✓</td>
      <td>Lossless and Lossy</td>
    </tr>
    <tr>
      <td>VNC</td>
      <td>Windows, Mac, Linux, iOS, Android</td>
      <td>VNC</td>
      <td>✗</td>
      <td>✓</td>
      <td>Tight and Zlib</td>
    </tr>
    <tr>
      <td>Guacamole HTML5 Client</td>
      <td>Windows, Mac, Linux, iOS, Android</td>
      <td>RDP, VNC, SSH</td>
      <td>✓</td>
      <td>✓</td>
      <td>JPEG, PNG</td>
    </tr>
    <tr>
      <td>noVNC Client</td>
      <td>Windows, Mac, Linux, iOS, Android</td>
      <td>VNC</td>
      <td>✓</td>
      <td>✓</td>
      <td>Tight, Hextile, CopyRect, Raw</td>
    </tr>
  </tbody>
</table>


### Diferències entre visors (al navegador i de client d'escriptori)

Se poden diferenciar dos tipus principals de visors:

* **Visors integrats al navegador**: el visor queda integrat dins d'una pàgina web. Des d'una pestanya del navegador es pot controlar el sistema operatiu dels escriptoris. La decodificació de la senyal de vídeo i l'enviament de la senyal del ratolí i del teclat es fa des d'una pestanya del navegador.

    * **Avantatges**:
        * No cal tenir un client instal·lat, funciona des de qualsevol dispositiu (ordinadors, tauletes, mòbils) i sistema operatiu que tingui un navegador.
    * **Inconvenients**:
        * La decodificació no és tan eficient com en un visor dedicat, es pot percebre una **certa lentitud** en l'actualització de l'escriptori i en treballar movent elements dins del visor.
        * No podem redirigir dispositius locals connectats per USB a l'escriptori virtual.

* **Aplicacions clients d'escriptori**: són aplicacions dedicades a funcionar com a visor, estan optimitzades i són la millor opció per tenir la millor experiència d'usuari.

    * **Avantatges**:
        * Optimitzades per als protocols de clients d'escriptori, tenen un millor rendiment que els visors integrats al navegador. Si no hi ha problemes de xarxa (latència i amplada de banda adequats), la sensació de treballar amb l'escriptori és similar a la d'un equip real.
        * Tot i què depèn del tipus de protocol i de la versió del client, en general aquests visors ofereixen opcions avançades com la redirecció de dispositius a través del port USB i arrossegar fitxers i portapapers des de l'escriptori virtual a la màquina host i viceversa.
    * **Inconvenients**:
        * No en tots els casos els clients vénen preinstal·lats amb el sistema operatiu, el que implica haver de realitzar una instal·lació de programari a l'equip host.
        * Algunes funcions avançades poden no estar disponibles segons les versions del client i del protocol.


### Ports i proxies

A IsardVDI s'ha realitzat un esforç significatiu per evitar obrir ports addicionals i encapsular les connexions en proxies HTTP. S'ha intentat adaptar la plataforma a qualsevol situació en la qual hi hagi un firewall que pugui bloquejar o dificultar les connexions. Per defecte, s'utilitzen els següents ports:

* TCP/**80** per al proxy on s'encapsulen les connexions del protocol **SPICE**
* TCP/**443** per al lloc web i els visors integrats **al navegador**
* TCP/**9999** per al proxy on s'encapsulen les connexions del protocol **RDP**


## FAQs - Preguntes freqüents

<details>
  <summary>No puc escanejar un dispositiu USB mitjançant visor RDP</summary>
  <p>Si l'usuari té un amfitrió Windows i intenta escanejar un dipositiu USB en un escriptori virtual sense èxit, es pot seguir <b><a href="../remote_fx.ca">aquest manual sobre l'ús de RemoteFX</a></b>.</p>
</details>