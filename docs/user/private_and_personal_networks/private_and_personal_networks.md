# Private and personal networks

## Explanation

A **private** network is configured on multiple desktops at once to establish communication between **multiple users** machines. 

*- Example use case: client-server architecture between desktops and between desktop and client machine.*

A **personal** network is configured on multiple desktops at once to establish a communication between **same user** machines. 

*- Example use case: client-server architecture where multiple clients use the same IP address.*

Both types of network can be **combined** into one desktop and virtual environment, as well as **Internet outgoing** cards. Isard virtual desktops also allow you to function as **routers** or **servers** that are networked with the WireGuard card (create a personal **VPN** connection) and thus be accessible from other desktops and/or from the client machine.

You can **modify** access permissions to these networks to limit which users or groups can use them.

*- Example Use Cases: Client architecture- DHCP server with two desktops respectively using a private network, allowing later use of a personal network by modifying the desktop and its networks.*


## User Manual

### Role advanced

#### Server Desktop

1. [**Create Desktop**](https://isard.gitlab.io/isardvdi-docs/user/create desktop.ca/#crear-desktop) taking base template from **Debian 11 Desktop** configured in Isard

![](./private_and_personal_networks.images/0vZ63DU.png)


2.- Configure networks (at the moment or [edit later](https://isard.gitlab.io/isardvdi-docs/user/edit),desktop.ca/#editar-desktop)), adding ****Defualt** and ** ibadia1’** (**bridge adapter** and **private** respectively)

![](./private_and_personal_networks.images/e4UOfKp.png)

![](./private_and_personal_networks.images/1V2ayAH.png)


3.- Boot desktop and [modify **network **configuration**](https://isard.gitlab.io/isardvdi-docs/user/edit'desktop.ca/#hardware) from both cards, corresponding first to ,**Default**' and second to  i**badia1**' (second card is configured with **fixed ip**)

![](./private_and_personal_networks.images/oihGAPD.png)

![](./private_and_personal_networks.images/VratvuJ.png)

![](./private_and_personal_networks.images/7J4O1kL.png)


' NOTE: The order of network cards that are configured on the desktop in Isard is the order of cards that appear configured on the desktop system, for example when the ```ip -c a``` command is written to the terminal.


4.- Upgrade repositories and **install package .apache2’**. Verify your operation

![](./private_and_personal_networks.images/p9xAC9K.png)


#### client desktop

1. [Create a desktop taking base template](https://isard.gitlab.io/isardvdi-docs/user/create'desktop.ca/#crear-desktop) from**Ubuntu 22.04 Desktop** configured in Isard

![](./private_and_personal_networks.images/pYKsB50.png)


2. [Configure networks](https://isard.gitlab.io/isardvdi-docs/user/edit),desktop.ca/#hardware), adding only the private ****badia1'**

![](./private_and_personal_networks.images/bRbf6WK.png)


3. Boot desktop and modify **network settings**

![](./private_and_personal_networks.images/rK3xhII.png)


- **Ping to server**:

![](./private_and_personal_networks.images/W3AOtwN.png)


4.- Open a **web browser** and verify access to the **web server** through its **IP address**

![](./private_and_personal_networks.images/H6rCT5L.png)


5.- **Stop** desktops *suitably* (from system, graphically or with commands)


6.- [Convert desktops to **plantilla**](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#plantilles), [enable them (make **visible**)](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#fer-visible-o-invisible) and [**share** with desired users](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#share) 

![](./private_and_personal_networks.images/Ej0xSKI.png)


- You can **make a template** also **client** and have the appropriate network settings already:

![](./private_and_personal_networks.images/qqO0Q8P.png)


### User Role

##### Desktops from Templates

Sign in to Isard and check visibility of templates created and shared by **professor**

![](./private_and_personal_networks.images/TNHdgNf.png)


. NOTE: creating a desktop, if not modified, inherits hardware configurations from the template. You can modify it when creating the desktop or once it has already been created with the edit button.


You can [**change**](https://isard.gitlab.io/isardvdi-docs/user/edit),desktop.ca/#editar-desktop), for example, the private network ****bay1’** over the **personpersonal-badia’** network to **server** and to **client**, so that the learner can perform the practice in a **exclusive network segment**, and so each learner can use the **same uncollisioned IP addresses**.

![](./private_and_personal_networks.images/gTwVVEH.png)

![](./private_and_personal_networks.images/dLx9n0z.png)

![](./private_and_personal_networks.images/iN98Rvq.png)

![](./private_and_personal_networks.images/dX5pLqa.png)

![](./private_and_personal_networks.images/FhUtMy6.png)

![](./private_and_personal_networks.images/b3gFOK2.png)