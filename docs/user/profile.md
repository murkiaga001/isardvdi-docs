# Profile

To go to the profile, press the drop-down menu ![](./profile.es.images/profile1.png)

![](./profile.images/profile1.png)

And the button ![](./profile.images/profile2.png)

![](./profile.images/profile3.png)

## Change language

To change the language, press the drop-down menu and choose the language

![](./profile.images/profile4.png)

![](./profile.images/profile5.png)

## Change password

To change the password, press the button ![](./profile.images/profile6.png)

![](./profile.images/profile7.png)

And a dialog box will appear where to fill in the form

![](./profile.images/profile8.png)

## See quotas

Quotas define the amount of resources that can be used based on the group to which the user belongs.

Here you can see all the resources that have been used.

![](./profile.images/profile9.png)