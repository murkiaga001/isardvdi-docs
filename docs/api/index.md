# Api endpoints

We are currently evaluating swagger documentation.

Meanwhile you can check endpoints at [api views](https://gitlab.com/isard/isardvdi/-/tree/main/api/src/api/views) and POST/PUT parameters in [cerberus](https://gitlab.com/isard/isardvdi/-/tree/main/api/src/api/schemas) schemas.

There is an api access example at [https://gitlab.com/isard/isardvdi/-/blob/main/api/src/scripts/test_one_endpoint.py](https://gitlab.com/isard/isardvdi/-/blob/main/api/src/scripts/test_one_endpoint.py)